#!/bin/bash
clear
 
echo ""
echo "#############################################################"
echo "# Automatically to  Install oh-my-zsh and initialize it    #"
#fi
#function checkOs(){
    #if [ -f /etc/RedHat-release ]
    #then
        #OS="CentOS"
    #elif [ ! -z "`cat /etc/issue | grep -i bian`" ]
    #then
        #OS="Debian"
    #elif [ ! -z "`cat /etc/issue | grep -i ubuntu`" ]
    #then
        #OS="Ubuntu"
    #else
        #echo "Not supported OS"
        #exit 1
    #fi
#}
read OS
echo OS: $OS
# Install zsh and git
function zsh_install(){
if [ "$OS" == 'CentOS' ]
then
    echo Install zsh and tmux
    sudo yum -y install zsh tmux
else
    echo Install zsh and tmux
    sudo apt-get -y install zsh tmux 
fi
zshPath="`which zsh`"
}
function zsh_config(){
  echo download zsh-syntax-highlighting
  git clone https://github.com/zsh-users/zsh-syntax-highlighting ~/.zsh/zsh-syntax-highlighting
  echo "source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" >> ~/.zshrc

  echo download zsh-autosuggestions
  git clone https://github.com/zsh-users/zsh-autosuggestions ~/.zsh/zsh-autosuggestions
  echo 'source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh' >> ~/.zshrc

  echo download zsh-powerlevel10k
  git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/.zsh/powerlevel10k
  echo 'source ~/.zsh/powerlevel10k/powerlevel10k.zsh-theme' >> ~/.zshrc

}
function tmux_config(){
  echo download tmux plugin manager
  git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
}
function vim_config(){
  echo generating soft link
  ln -sf $PWD/nvim ~/.config/nvim
  ln -sf ~/.config/nvim/init.nvim ~/.vimrc
  ln -sf $PWD/.tmux.conf ~/.tmux.conf
}
function main(){
#zsh_install
zsh_config
#tmux_config
vim_config
}
main
