call plug#begin('$HOME/.config/nvim/plugged') 
"call plug#begin(stdpath('data') . '/plugged') 
"Plugin Section
" Treesitter
"Plug 'nvim-treesitter/nvim-treesitter'
"Plug 'nvim-treesitter/playground'

" Auto Complete
"Plug 'neoclide/coc.nvim', {'branch': 'release'}

"Plug 'preservim/nerdtree'

Plug 'sirver/ultisnips'
Plug 'honza/vim-snippets'

Plug 'lervag/vimtex'

Plug 'joshdick/onedark.vim'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'


Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

Plug 'preservim/nerdcommenter'

Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'neoclide/coc-vimtex'
Plug 'neoclide/coc-json'
Plug 'neoclide/coc-css'
if g:is_nvim
"  Plug 'neovim/nvim-lspconfig'
"  Plug 'hrsh7th/nvim-compe'
endif
call plug#end()

colorscheme onedark
